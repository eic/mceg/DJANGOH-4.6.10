###################################################
#                                                 #
#   Makefile for package djangoh 4.6.9            #
#   interfaced via PYTHIA to LHAPDF-5.8.6         #
#                                                 #
#   Type mismatch for common block /LUDAT1/       #
#    between lhaglue and pythia requires          #
#    recompilation of lhaglue                     #
#                                                 #
#   Version for use at BNL: eic000*               #
#   32 bit                                        #
#                                                 #
#   HS: 01.11.2011                                #
#   last mod: 07.08.2013 (HS)                     #
#                                                 #
###################################################


# Adjust path to install dir:
#DJANGOH=/direct/eic+u/hspiesb/projects/djangoh
#DJANGOH=/afs/rhic.bnl.gov/eic/PACKAGES/djangoh.4.6.10bnl/

CXX=gfortran
CFFLAGS = -m64 -g -fno-automatic -std=legacy

PROGRAM=djangoh
LHAPDF=$(LHAPDF5)
CERN=$(CERN_ROOT)/lib

LIBS=-L$(LHAPDF) -L$(LHAPDF)/lib -lLHAPDF \
     -L$(CERN) -lmathlib -lkernlib -lpacklib_noshift


OBJS = $(addsuffix .o, $(basename $(SRCS)))

SRCS = djangoh_h.f djangoh_l.f djangoh_u.f djangoh_t.f \
	sophia.f gdfit10.f pythia-6.4.28.f polpdf.f jetset7410.f

all: djangoh

%.o: %.f
	$(CXX) $(CFFLAGS) -o $@ -c $<

lhaglue.o: lhaglue-5.9.1-copy.f
	$(CXX) $(CFFLAGS) -ffree-form -c lhaglue-5.9.1-copy.f -o lhaglue.o


gmc_random.o: gmc_random.f
	$(CXX) $(CFFLAGS) -ffree-form -c gmc_random.f -o gmc_random.o


djangoh: $(OBJS) lhaglue.o gmc_random.o
	$(CXX) $(CFFLAGS) -o $@ $(OBJS) lhaglue.o gmc_random.o $(LIBS)


clean:
	rm -f $(OBJS) $(PROGRAM)
	rm -f lhaglue.o gmc_random.o

install: djangoh
	@echo INSTALLING
	cp -v $^ $(EICDIRECTORY)/bin

.PHONY: clean all install
