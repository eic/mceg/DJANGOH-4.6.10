*----------------------------------------------------
*     created by Lara De Nardo, October 28, 2010
*     output is F2
*----------------------------------------------------

c-hs  renamed: name may not contain '-'
c     HS (21.01.201)
      DOUBLE PRECISION FUNCTION gdfit10(dx,dQ2,dW2,target)
      
      IMPLICIT NONE

      CHARACTER*1 TARGET
      DOUBLE PRECISION PARAM(23)
      DOUBLE PRECISION F2P,F2R
      DOUBLE PRECISION XP,XR,CP,CR,AP,AR,BP,BR,t,dQ2,dx,dW2

c-hs  this was undefined in the original version
c     added by HS (21.01.2011)
      double precision aprmass
      aprmass=938.28D-3
c-hs
      
      if (target.eq.'p') then

         param(  1)=       0.504875
         param(  2)=      34.046472
         param(  3)=       0.066144
         param(  4)=       1.528063
         param(  5)=       0.065270
         param(  6)=      -0.119775
         param(  7)=      -0.490448
         param(  8)=       1.287539
         param(  9)=       0.984561
         param( 10)=       2.604510
         param( 11)=       1.404179
         param( 12)=       0.358903
         param( 13)=       0.114424
         param( 14)=       1.120841
         param( 15)=       0.336422 
         param( 16)=       1.113998
         param( 17)=       0.598313
         param( 18)=      -7.908678
         param( 19)=      12.523172
         param( 20)=       0.112027
         param( 21)=       1.051316
         param( 22)=       2.739961
         param( 23)=       1.818436

      elseif(target.eq.'d') then

         param(  1 )=               0.455872      
         param(  2 )=               0.531614      
         param(  3 )=               2.719053      
         param(  4 )=              17.494116      
         param(  5 )=               0.065270      
         param(  6 )=              -0.135367      
         param(  7 )=              -0.909263      
         param(  8 )=               0.677523      
         param(  9 )=              -6.934579      
         param(  10)=               0.400460      
         param(  11)=               0.067910      
         param(  12)=               0.175499      
         param(  13)=               0.706134      
         param(  14)=               2.062847      
         param(  15)=               0.654724      
         param(  16)=               6.036018      
         param(  17)=               1.173878      
         param(  18)=              -1.196548      
         param(  19)=               9.159108      
         param(  20)=               0.222962      
         param(  21)=               0.575077      
         param(  22)=              31.152266      
         param(  23)=               1.488237      
      else
         write(*,*) 'target unknown- STOP'
         stop
      endif
      
      t=dlog((dQ2+param(4))/param(5)) / log(param(4)/param(5))
      t=dlog(t)

      if (dQ2.ne.0.d0) then         
         XP=(dQ2+param(2))/(dQ2/dx+param(2))
         XR=(dQ2+param(3))/(dQ2/dx+param(3))
      else
         XP=1.d0+(dW2-aprmass*aprmass)/(dQ2+param(2))
         XR=1.d0+(dW2-aprmass*aprmass)/(dQ2+param(3))
         XP=1.d0/XP
         XR=1.d0/XR
      endif

      AP=param( 6)+(param( 6)-param( 7))*(1.d0/(1.d0+t**param( 8))-1.d0)
      CP=param(12)+(param(12)-param(13))*(1.d0/(1.d0+t**param(14))-1.d0)

      BP=param( 9)+param(10)*t**param(11)
      AR=param(15)+param(16)*t**param(17)
      BR=param(18)+param(19)*t**param(20)
      CR=param(21)+param(22)*t**param(23)

      F2P=CP*(XP**AP)*(1.d0-dX)**BP
      F2R=CR*(XR**AR)*(1.d0-dX)**BR
      
      gdfit10=(F2P+F2R)*dQ2/(dQ2+param(1))
      
      return
      end
