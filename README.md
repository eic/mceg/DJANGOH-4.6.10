# DJANGOH
The event generator DJANGOH simulates deep inelastic lepton-proton scattering for both NC and CC events including both QED and QCD radiative effects.
DJANGOH contains the Monte Carlo program HERACLES and an interface of HERACLES to LEPTO.
The use of HERACLES allows to take into account the complete one-loop electroweak radiative corrections and radiative scattering.
The LUND string fragmentation as implemented in the event simulation program JETSET is used to obtain the complete hadronic final state. At low hadronic mass, SOPHIA is used instead of LEPTO.
DJANGOH comprises the programs (formerly kept separately) DJANGO6 and HERACLES. The interface is to version 6.5.1 of LEPTO.
For eRHIC DJANGOH was upgraded to use nuclear PDFs as available in LHAPDF
From version 4.6.10 on DJANGOH simulates also longitudinal polarised deep inelastic lepton-proton scattering for both NC and CC events including both QED and QCD radiative effects.

Author: Hubert Spiesberger

# Installation

Can be built using "make".
The "install" target should be customized to your environment before using

# Running

NOTE: LHAPDF pdfset locations are
in the STEER-FILE, adjust according to your environment
You can then test with things like:
```
cp  $EICDIRECTORY/PACKAGES/DJANGOH-4.6.10/STEER-FILES/updated.erhic.eAu.Rad=0-nlo+fl+frag.NC.in .
$EICDIRECTORY/djangoh < updated.erhic.eAu.Rad=0-nlo+fl+frag.NC.in
```

# Known issues:
Many warnings are generated during compilation, specifically
"Padding of 4 bytes required..."
and
"Warning: Actual argument contains too few elements for dummy argument ..."
Author is contacted.
 



